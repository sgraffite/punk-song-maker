extends Button

@onready var label: Label = $Label

const MIN_VALUE = -1
var value: int = MIN_VALUE: set = _set_value
var max_value = 10


func _set_value(_value: int):
	if(_value < MIN_VALUE):
		_value = max_value
	elif(_value > max_value):
		_value = MIN_VALUE

	value = _value
	update_label()


func update_label() -> void:
	if(value < 0):
		label.text = ''
		return
	
	label.text = str(value)

func increment_value():
	value = value + 1

func decrement_value():
	value = value - 1
