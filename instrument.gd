class_name Instrument extends HBoxContainer

const BEAT_BUTTON = preload("res://beat_button.tscn")
var total_beats = 10 #TODO: make into sound array?

var beats = []

func _ready() -> void:
	for i in 3:
		var beatButton = BEAT_BUTTON.instantiate()
		add_child(beatButton)
		beatButton.max_value = total_beats
		beats.append(beatButton)
		beatButton.gui_input.connect(self.beat_clicked.bind(beatButton))
		
	print("ready")

func beat_clicked(event, button):
	if(event is InputEventMouseButton && event.pressed):
		if(event.button_index == MOUSE_BUTTON_LEFT):
			print("button index=" + str(beats.find(button)))
			button.increment_value()

		if(event.button_index == MOUSE_BUTTON_RIGHT):
			print("button index=" + str(beats.find(button)))
			button.decrement_value()
